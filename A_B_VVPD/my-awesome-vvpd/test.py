import random


def create_matrix():
    """
    Создает рандомную матрицу

    Returns:
        Матрица размером от 5x5 до 10x10, заполненная
        рандомными числами

    Examples:
        >>> create_matrix()
        [[185, 48, 190, 128, 16, 11],
        [214, 254, 170, 73, 219, 99],
        [210, 173, 90, 204, 148, 209],
        [189, 186, 173, 25, 84, 235],
        [224, 126, 43, 104, 25, 47]]
    """
    LEFT_BORDER = 5
    RIGHT_BORDER = 10

    rows = random.randint(LEFT_BORDER,RIGHT_BORDER)
    cols = random.randint(LEFT_BORDER,RIGHT_BORDER)

    matrix = [[random.randint(0, 256) for i in range(cols)] for i in range(rows)]
    return matrix


print(create_matrix())
