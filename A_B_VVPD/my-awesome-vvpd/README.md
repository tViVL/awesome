# Практика 5 ввпд
Практическая номер 5 (в которой необходимо было создать README.md файл по любому коду, написанному в течение семестра). 
## Оглавление

[Ссылка](https://ya.ru)

#### Маркированный список
* Первый пункт
* Второй пункт
* ...
* N-ый пункт

#### Список с чек-боксами
* [ ] Первый пункт с чек-боксом
* [ ] Второй пункт с чек-боксом
* [ ] Третий пункт с чек-боксом
* [ ] Четвертый пункт с чек-боксом

#### Пример формулы
$`(x**2 + y**2 - 1)**3 - x**2 * y**3 = 0`$

# Первая функция
```python
def greedy_alg(phone_operator_regions, regions):
    """
    Жадный алгоритм:
    а) выбрать оператора, покрывающего наибольшее количество
    регионов, и еще не входящих в покрытие. Если оператор будет покрывать
    некоторые регионы, уже входящие в покрытие, это нормально;
    б) повторять, пока остаются непокрытые элементы множества

    Args:
        phone_operator_regions: словарь, ключами которого являются названия операторов,
            а значениями - список регионов, покрываемых этим оператором

        regions: множество всех регионов, которые необходимо покрыть

    Returns:
        множество операторов сотовой связи, которые
            работают во всех указанных регионах

    Examples:
        >>> greedy_alg({'MTS': {4, 5, 7, 9},
                        'MEGAFON': {1, 2, 3, 4, 5, 6},
                        'YOTA': {1, 4, 5, 10, 11}}, 
                        {1, 2, 3, 4, 5, 7, 11})
        
        ['MEGAFON', 'MTS', 'YOTA']
    """
    phone_operator = list(phone_operator_regions)
    answer = []
    while regions != set():
        hit_max = name_of_max = 0
        for i in range(len(phone_operator)):
            hits = len(phone_operator_regions[phone_operator[i]] & regions)
            if hits > hit_max:
                hit_max = hits
                name_of_max = phone_operator[i]
        try:
            regions = regions.difference(phone_operator_regions[name_of_max])
            del phone_operator_regions[name_of_max]
            phone_operator.remove(name_of_max)
            answer.append(name_of_max)
        except KeyError:
            return 'Один из регионов не входит в покрытие всех операторов'
    print(answer)
```

_Пример текста курсивом_

## Вставка изображения
![Пример изображения](A_B_VVPD/my-awesome-vvpd/d1a384244762d7aa81ea6ecd7c28e102d427c0dcr1-1600-1058v2_uhq.jpg)

